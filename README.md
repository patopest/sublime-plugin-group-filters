# Group Filters

This Sublime Text plugin enables to automatically order Groups of tabs based on the file type.  

## Usage

Once installed, the `Group Filters` sub-menu will be available in your window's `View` Menu.

### Features

- Order all the tabs based on the provided filters.
- Move a tab on open to the correct group.


### Settings

The plugin uses the default settings found in the [Preferences](./Preferences.sublime-settings) file.  

However you can override those in multiple ways:

- Override these settings globally in your User Preferences file. These settings will then apply to your whole Sublime Text. Click on `Sublime Text -> Preferences -> Settings`.
- Override these settings on a per-project basis. Click on `Project -> Edit Project`. In the the `.sublime-project` file you can add a `settings` section:

```json
	"settings":
	{
		/* Add here any settings you want to override for the project */

		"group_filters": {
			"0": [".c", ".py"], 
			"1": [".h", ".sublime*"]
		}
	}
```

## Installation

This package is not in Package Control yet.  
Clone this repository in your local packages folder.  
If you are unsure as to where your local packages directory is, please consult the [docs](https://docs.sublimetext.io/guide/getting-started/basic-concepts.html#the-data-directory).  

Example (on MacOS):

```shell
cd ~/Library/Application\ Support/Sublime\ Text/Packages
git clone git@gitlab.com:patopest/sublime-plugin-group-filters.git Group\ Filters
``` 


## Useful Docs

- Official Sublime [docs](https://www.sublimetext.com/docs/index.html) and the [API reference](https://www.sublimetext.com/docs/api_reference.html)
- Unofficial Sublime [docs](https://docs.sublimetext.io/)
- [Origami](https://github.com/SublimeText/Origami) package, a good example of a powerful plugin


## TODO
- Add the ability for multiple layout configs
- Figure out menu config and options
- Ability to filter only one tab?
- Apply to all views not just file loads (i.e.: when opening a second tab on an already loaded file)
- Tab order/priority and sorting when reordering all the tabs
- More granularity in rules (i.e: README or main.c first, ...)