import os
import re
import sublime
import sublime_plugin


layouts_command = {
	"command": "set_layout",
	"caption": "Columns: 2",
	"args":
	{
		"cols": [0.0, 0.5, 1.0],
		"rows": [0.0, 1.0],
		"cells": [[0, 0, 1, 1], [1, 0, 2, 1]]
	}
}


def compute_pattern(pattern):
		pattern = pattern + "$" 				# End of line anchor
		pattern = pattern.replace('.', '\\.') 	# Adding backslash to avoid regex . expression (any character)
		pattern = pattern.replace('*', '.*') 	# Change from bash style to python re style to make it work
		return pattern


def get_group(view, settings):
	filename = os.path.basename(view.file_name())
	file, extension = os.path.splitext(filename)

	for group, exts in settings.get("group_filters").items():
		group_idx = int(group)
		for ext in exts:
			pattern = compute_pattern(ext)
			match = re.search(pattern, extension)
			
			if match:
				return group_idx

	return None

def change_layout(window):
	num_groups = window.num_groups()
	if (num_groups < 2):
		window.set_layout(layouts_command["args"])


class ReorderViewsCommand(sublime_plugin.WindowCommand):

	def run(self):
		# print(self.window)
		settings = self.window.active_view().settings()

		if (settings.get("group_filters_change_layout", None)):
			change_layout(self.window)

		views = self.window.views()
		for view in views:
			group_idx = get_group(view, settings)

			if group_idx is not None:		
				views_in_group = len(self.window.views_in_group(group_idx))
				self.window.set_view_index(view, group_idx, views_in_group)


class OnFileOpen(sublime_plugin.EventListener):

	def on_load(self, view):
		window = view.window()
		settings = view.settings()

		if (settings.get("group_filters_on_load", False)) is False:
			return None

		if (settings.get("group_filters_change_layout", None)):
			change_layout(window)

		group_idx = get_group(view, settings)
		if group_idx is not None:
			views_in_group = len(window.views_in_group(group_idx))
			# print("Moving view {} to {},{}".format(view.file_name(),group_idx,views_in_group))
			window.set_view_index(view, group_idx, views_in_group)


